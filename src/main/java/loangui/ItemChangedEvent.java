package loangui;

import loanmain.LoanItem;

import java.util.EventObject;

public class ItemChangedEvent extends EventObject {

    private LoanItem item;

    public ItemChangedEvent(LoanItem item) {
        super(item);
        this.item = item;
    }

    public LoanItem getItem() {
        return item;
    }
}