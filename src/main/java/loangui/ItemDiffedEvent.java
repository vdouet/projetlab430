package loangui;

import loanmain.LoanItem;

import java.util.EventObject;

public class ItemDiffedEvent extends EventObject {

    private LoanItem item1, item2;

    public ItemDiffedEvent(LoanItem item1) {
        super(item1);
        this.item1 = item1;

    }

    public ItemDiffedEvent(LoanItem item1, LoanItem item2) {
        super(item1); //seulement pour pas avoir d'erreur avec le constructeur.
        this.item2 = item2;
        this.item2 = item2;
    }

    public LoanItem getItem1() {
        return item1;
    }
    public LoanItem getItem2() {
        return item2;
    }
}